# Gene fusion pipeline

## Description

This project is a Nextflow bioinformatics pipeline for RNAseq data analysis of leukemia patients with the emphasis on fusion gene detection. 

The pipeline includes preprocessing, gene fusion detection and validation with postprocessing, resulting in a summary table of detected fusion gene candidates, which combines the results of Jaffa, Arriba, STAR-Fusion, Cicero and Fusioncatcher as seen below:

![image info](images/workflow.png)

## Installation and Requirements

The analysis can be executed either locally or with Metacentrum with Kubernetes. To run the pipeline with a different executor, please modify the config file according to the Nextflow documentation (e.g. with `-c` parameter).

### Local execution requirements
- Minimal RAM: 45
- Minimal CPU: 1
- Installed Nextflow - please follow the instruction at https://nf-co.re/docs/usage/installation

### Metacentrum execution requirements

We provide a prepared configuration file that anables running the analysis with Metacentrum with Kubernetes. In this case the pipeline will not run on that computer it will run in Kubernetes cluster and the computer serves only for starting it.

Please follow the instruction at https://docs.cerit.io/docs/nextflow.html. 

To run the pipeine it is needed to have:
- Installed Nextflow
- Installed kubectl with kubeconfig file in an expected location $HOME/.kube/config
- Kubernetes Namespace to run in 
- Created PVC with:
    - Minimal RAM: 100 GB
    - Minimal CPU: 20

## Input files:

- Reads files, containing the paired end sequencing data to be analysed, where the only supported extension is `.fastq.gz.` File naming is required to match one of the following patterns:
    -	If there are only two files (read 1 and read 2) per sample, they need to match the pattern `*_R1,_R2.fastq.gz`, for example, `test_data_R1.fastq.gz`.
    -	In case there are multiple files from different lanes per one sample, the files are expected to have the typical Illumina naming convention matching the regular expression `^(.+_S[0-9]+)+(_.+)*_R([1-2])_`, for example `p4_S4_L001_R1_001.fastq.gz`
    It is possible to provide reads for multiple samples and the analysis will be conducted on each of them parallelly, exporting results to separate directories for each sample.

- Reference files for each of the tools that request them. We provide an already prepared directory containing all the required files (available at https://owncloud.cesnet.cz/index.php/s/XVovHksT8m1hIMa), however, it is possible to provide a custom directory as long as it follows the same directory structure.


## Usage

Local execution:
```
nextflow run https://gitlab.com/souckmi/gene-fusion-pipeline <ARGUMENTS>
```

Metacentrum execution:
```
nextflow kuberun https://gitlab.com/souckmi/gene-fusion-pipeline -profile metacentrum <ARGUMENTS>
```
where the arguments for both executors are:
- `-r` which defines the project revision intended to be executed, since the `latest` is not always reliable, so please choose the latest particular revision 
- `-profile` defines the Nextflow profile - this parameter can be omitted or set to `standard`in case of local execution. In case of using Metacentrum, set the value to `metacentrum`
- `--readsdir` defines the path to a directory with input FASTQ files to be analysed (required)
- `--runId` defines the run id, set by the user - does not affect the analysis but is part of the name of the output files (required)
- `--mergeInputFiles` indicates wheather the input FASTQ files are from different lanes and need to be merged (default False)
- `--reference` defines the path to direcotry containing the reference files, as described in Input files section (required)
- `--outdir` defines the output directory (required)

and arguments for Metacentrum execution:
- `-pod-image` if running the pipeline with Metacentrum, please use 'cerit.io/nextflow/nextflow:22.06.1' (required)
- `-v` mounts the PVC and its content, please specify the name of the PVC. It is recommended to leave the `:/mnt`. e.g. `<storage claim name>:/mnt` (required)
- `--k8s_namespace` defines the Kubernetes Namespace to run in (required)
- `--k8s_storageclaimname` defines the name of the PVC (required)
- `--k8s_storagemountpath` defines the path in mount. It is the path after `:` defined in value above, therefore perhaps `/mnt` if left unchanged (required)
- `--k8s_launchdir` defines the path of a launch directory from storage mount path e.g. '/mnt/some/path' (required)
- `--k8s_workdir`defines the path of a working directory from storage mount path e.g. '/mnt/some/path' (required)

Example of local execution:
```
nextflow run https://gitlab.com/souckmi/gene-fusion-pipeline \
    -r revisionNumber \
    --readsdir /path/to/data/directory \
    --runId myRunId
    --mergeInputFiles true \
    --reference /path/to/reference/directory \
    --outdir /path/to/output/directory 
```

Example of metacentrum execution:
```
nextflow kuberun https://gitlab.com/souckmi/gene-fusion-pipeline \
    -r revisionNumber \
    -profile metacentrum
    -pod-image 'cerit.io/nextflow/nextflow:22.06.1'\
    -v <storage claim name>:/mnt\
    --readsdir /path/to/data/directory \
    --runId myRunId
    --mergeInputFiles true \
    --reference /path/to/reference/directory \
    --outdir /path/to/output/directory \
    --k8s_namespace <k8s name space> \
    --k8s_storageclaimname <storage claim name> \
    --k8s_storagemountpath /mnt \
    --k8s_launchdir /mnt/path/to/launchdir \
    --k8s_workdir /mnt/path/to/workDir \
```

## Output files

Most of the files produced by each process are included in the output directory. The main results of the analysis are `final_result_table.xlsx` in the `<sample>`folder and visualisations produced by Fastp at `<sample>/fastp/*report.html`, FusionInspector at `<sample>/fusioninspector/*fusion_inspector_web.html` and Arriba's visualisation of FusionInspector's result at `<sample>/fusioninspector/visualisation/fusions.pdf`.

Every file produced by a process is renamed so that it follows the pattern `<sample_name>_<run_id>_<reference_version>_<caller>_<original_file_name>`.
Therefore, for example, if the input FASTQ file carries the name “p1_S4_L001_R1_001.fastq.gz” and the run id provided by the user is “RUN1”, a file produced by Arriba originally named “fusions.tsv” would be named “p1_S4_RUN1_hg38_arriba_fusions.tsv”.

The outut folder has the following stucture:

```
├───p4_S4
│   │   p4_S4_RUN1_hg38_final_result_table.csv
│   │   p4_S4_RUN1_hg38_final_result_table.xlsx
│   ├───arriba
│   │       ...
│   │       p4_S4_RUN1_hg38_arriba_fusions.tsv
│   ├───cicero
│   │   │   ...
│   │   └───p4_S4_RUN1_hg38_cicero_CICERO_DATADIR
│   │       └───sorted_Aligned.out
│   │               ...
│   │               sorted_Aligned.out.final_fusions.txt
│   ├───fastp
│   │       p4_S4_RUN1_hg38_fastp_2573_R1_hg38_fastp_report.html
│   │       p4_S4_RUN1_hg38_fastp_demultiplexed_trimmed_2573_R1.fastq.gz
│   │       p4_S4_RUN1_hg38_fastp_demultiplexed_trimmed_2573_R2.fastq.gz
│   ├───fusioncatcher
│   │       p4_S4_RUN1_hg38_fusioncatcher_final-list_candidate-fusion-genes.vcf
│   │       ...
│   ├───fusioninspector
│   │   │   p4_S4_RUN1_hg38_fusioninspector_FusionInspector.fusions.abridged.tsv.annotated.coding_effect
│   │   │   p4_S4_RUN1_hg38_fusioninspector_finspector.fusion_inspector_web.html
│   │   │   ...
│   │   └───visualization
│   │           p4_S4_RUN1_hg38_fusioninspector_fusions.pdf
│   ├───jaffa
│   │   │   ...
│   │   └───p4_S4_RUN1_hg38_jaffa_results.csv
│   ├───samtools
│   │       p4_S4_RUN1_hg38_samtools_sorted_Aligned.out.bam
│   │       p4_S4_RUN1_hg38_samtools_sorted_Aligned.out.bam.bai
│   ├───star
│   │       p4_S4_RUN1_hg38_star_Aligned.out.bam
│   │       p4_S4_RUN1_hg38_star_Chimeric.out.junction
│   │       ...
│   └───starfusion
│           ...
│           p4_S4_RUN1_hg38_starfusion_star-fusion.fusion_predictions.tsv
├───merged_input_files
│       p4_S4_R1.fastq.gz
│       p4_S4_R2.fastq.gz
└───nextflow_reports
        report.html
        timeline.html
```

### final_result_table.xlsx

This file contains a formatted summary result of all the fusion gene callers. Below are shown the relations between the columns of the new final result table and the columns of callers output files.

![image info](images/columns.png)

Description of the columns of the final result table is shown below, however, the columns group values outputed by the callers that are comparable, but their litteral meaning can slightly differ, so please reffer to the callers documentation.

| Column                | Description  |
|-----------------------|--------------|
| caller                | The bioinformatic tool whose results are shown in the row    |
| gene1                 | Gene symbol of the 5' end   |
| gene2                 | Gene symbol of the 3' end    |
| chrom1/position1      | Chromosomal position of the breakpoint in gene1    |
| chrom2/position2      | Chromosomal position of the breakpoint in gene2    |
| spanning_reads        | The number of reads which cover the breakpoint    |
| spanning_pairs        | The number of read-pairs, where each read in the pair aligns entirely on either side of the breakpoint    |
| coverage1/coverage2   | The coverage near the breakpoint in gene1/gene2    |
| annotation            | A simplified annotation for fusion transcript, depending on the caller. It mostly contains database(s) in which the fusion was found    |
| confidence            | Suggests the credibility of predictions    |
| prediction_effect     | Provides information on the location of the breakpoints. It indicates whether the proposed breakpoint occurs at reference exon junctions    |
| anchor                | Indicates whether there are split reads that provide 'long' alignments on both sides of the putative breakpoint    |
| FAR_left              | Left fusion allelic ratio (only for FusionInspector)    |
| FAR_right             | Right fusion allelic ratio (only for FusionInspector)    |
| FFPM                  | Normalised measure of the quantity of RNAseq fragments supporting the fusion event as: fusion fragments per total million RNAseq fragments (only for FusionInspector)    |

The table is formatted as follows:

| Rule                                                                                                          | Format        |
|---------------------------------------------------------------------------------------------------------------|---------------|
| The spanning reads value is less than 5                                                                       | Red cell      |
| The coverage1 is greater than 50                                                                              | Red cell      |
| The coverage2 is greater than 50                                                                              | Red cell      |
| The value in clumn chrom1 and chrom2 are equal                                                                | Both cells yellow  |
| The confidence column suggests low confidence (i.e. contains ‘LowConfidence’, ‘LQ’ or ‘low’)                  | Red row       |
| The prediction effect contains 'intragenic', 'intron', 'out-of-frame' or 'INCL_NON_REF_SPLICE'                | Red row       |
| The annotation column includes one of the sources that suggest that the fusion is relevant to cancer biology* | Green row     |
| The annotation column includes one of the sources that suggest that the fusion pair may not be relevant to cancer, and be potential false positive**  |  Red row  |
| In the caller column, if the caller is ‘fusioninspector’ (to highlight the validation result)                 | Blue cell     |

\* The sources are 'Mitelman', 'chimerdb_omim', 'chimerdb_pubmed', 'ChimerKB', 'ChimerPub', 'ChimerSeq', 'Cosmic', 'YOSHIHARA_TCGA', 'Klijn_CellLines', 'Larsson_TCGA', 'CCLE', 'HaasMedCancer', 'GUO2018CR_TCGA', 'TumorFusionsNAR2018', 'TCGA_StarF2019', 'CCLE_StarF2019', 'DEEPEST2019', and in addition the value ‘Yes’ represents that the fusion was reported in the Mitelman database, according to Jaffa. The selection is based on CTAT_HumanFusionLib 
\*\* The sources are 'GTEx_recurrent_StarF2019', 'BodyMap', 'DGD_PARALOGS', 'HGNC_GENEFAM', 'Greger_Normal', 'Babiceanu_Normal', 'ConjoinG'. The selection is based on CTAT_HumanFusionLib 

## Authors and acknowledgment
This project was developed as part of a thesis at Czech Technical University in Prague by bc. Michaela Součková.


