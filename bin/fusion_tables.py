#!/usr/bin/env python

import pandas as pd
import numpy as np
import argparse

def create_result_table(arriba, fusioncatcher, starfusion, jaffa, cicero):
    
    # files to dataframes
    df_arriba = pd.read_csv(arriba, sep="\t")
    df_fusioncatcher = pd.read_table(fusioncatcher, sep="\t")
    df_starfusion = pd.read_csv(starfusion, sep="\t")
    df_jaffa = pd.read_csv(jaffa)
    df_cicero = pd.read_csv(cicero, sep="\t")

    # arriba
    cols = ['#gene1', 'gene2']
    df_arriba["fusions"] = df_arriba[cols].apply(lambda row: '--'.join(row.values.astype(str)), axis=1)
    np.savetxt(r'arriba.txt', df_arriba["fusions"].values, fmt='%s')

    # fusioncatcher
    cols = ['Gene_1_symbol(5end_fusion_partner)', 'Gene_2_symbol(3end_fusion_partner)']
    df_fusioncatcher["fusions"] = df_fusioncatcher[cols].apply(lambda row: '--'.join(row.values.astype(str)), axis=1)
    np.savetxt(r'fusioncatcher.txt', df_fusioncatcher["fusions"].values, fmt='%s')

    # starfusion
    np.savetxt(r'starfusion.txt', df_starfusion["#FusionName"].values, fmt='%s')

    # jaffa
    df_jaffa['fusion genes'] = df_jaffa['fusion genes'].str.replace(":", "--")
    np.savetxt(r'jaffa.txt', df_jaffa["fusion genes"].values, fmt='%s')

    # cicero
    cols = ['geneA', 'geneB']
    df_cicero["fusions"] = df_cicero[cols].apply(lambda row: '--'.join(row.values.astype(str)), axis=1)
    np.savetxt(r'cicero.txt', df_cicero["fusions"].values, fmt='%s')


if __name__ == "__main__":
   parser = argparse.ArgumentParser()
   parser.add_argument('-a', '--arriba')
   parser.add_argument('-f', '--fusioncatcher')
   parser.add_argument('-s', '--starfusion')
   parser.add_argument('-j', '--jaffa')
   parser.add_argument('-c', '--cicero')
   args = parser.parse_args() 
   create_result_table(args.arriba, args.fusioncatcher, args.starfusion, args.jaffa, args.cicero)
    