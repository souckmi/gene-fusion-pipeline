#!/usr/bin/env python

import pandas as pd
import argparse

def create_final_result_table(arriba, fusioncatcher, starfusion, jaffa, cicero, fusioninspector, outfile):
    
    # files to dataframes
    df_arriba = pd.read_csv(arriba, sep="\t")
    df_fusioncatcher = pd.read_table(fusioncatcher, sep="\t")
    df_starfusion = pd.read_csv(starfusion, sep="\t")
    df_jaffa = pd.read_csv(jaffa)
    df_cicero = pd.read_csv(cicero, sep="\t")
    df_fusioninspector = pd.read_csv(fusioninspector, sep="\t")

    result_cols = ["caller", 
                "gene1", 
                "gene2", 
                "chrom1", 
                "position1", 
                "chrom2", 
                "position2", 
                "spanning_reads",
                "spanning_pairs",	
                "coverage1", 
                "coverage2", 
                "annotation", 
                "confidence", 
                "prediction_effect", 
                "anchor",
                "FAR_left",
                "FAR_right",
                "FFPM"
                ]

    df_result = pd.DataFrame(columns=result_cols)

    # Arriba
    for index, row in df_arriba.iterrows():
        result_row = pd.Series({  
                        "caller": "arriba", 
                        "gene1": row['#gene1'], 
                        "gene2": row['gene2'], 
                        "chrom1": row['breakpoint1'].split(":")[0], 
                        "position1": row['breakpoint1'].split(":")[1], 
                        "chrom2": row['breakpoint2'].split(":")[0], 
                        "position2": row['breakpoint2'].split(":")[1], 
                        "spanning_reads": row['split_reads1'] + row['split_reads2'], 
                        "spanning_pairs": row['discordant_mates'],	
                        "coverage1": row['coverage1'], 
                        "coverage2": row['coverage2'], 
                        "annotation": row['tags'], 
                        "confidence": row['confidence'], 
                        "prediction_effect": row['site1'] + ', ' + row['site2'], 
                        "anchor": None,
                        "FAR_left": None,
                        "FAR_right": None,
                        "FFPM": None
                    })
        df_result = pd.concat([df_result, result_row.to_frame().T], ignore_index=True)

    # Starfusion
    for index, row in df_starfusion.iterrows():
        result_row = pd.Series(
                        {  "caller": "starfusion", 
                        "gene1": row['#FusionName'].split('--')[0], 
                        "gene2": row['#FusionName'].split('--')[1], 
                        "chrom1": row['LeftBreakpoint'].split(":")[0],
                        "position1": row['LeftBreakpoint'].split(":")[1], 
                        "chrom2": row['RightBreakpoint'].split(":")[0], 
                        "position2": row['RightBreakpoint'].split(":")[1], 
                        "spanning_reads": row['SpanningFragCount'],
                        "spanning_pairs": row['JunctionReadCount'],	
                        "coverage1": None, 
                        "coverage2": None, 
                        "annotation": row['annots'], 
                        "confidence": None, 
                        "prediction_effect": row['SpliceType'], 
                        "anchor": row['LargeAnchorSupport'],
                        "FAR_left": None,
                        "FAR_right": None,
                        "FFPM": None
                    })
        df_result = pd.concat([df_result, result_row.to_frame().T], ignore_index=True)
        
    # Jaffa
    for index, row in df_jaffa.iterrows():
        result_row = pd.Series({  
                        "caller": "jaffa", 
                        "gene1": row['fusion genes'].split(":")[0], 
                        "gene2": row['fusion genes'].split(":")[1], 
                        "chrom1": row['chrom1'],
                        "position1": row['base1'], 
                        "chrom2": row['chrom2'], 
                        "position2": row['base2'], 
                        "spanning_reads": row['spanning reads'],
                        "spanning_pairs": row['spanning pairs'],	
                        "coverage1": None, 
                        "coverage2": None, 
                        "annotation": row['known'], 
                        "confidence": row['classification'], 
                        "prediction_effect": str(row['inframe']) + ", " + str(row['aligns']), 
                        "anchor": None,
                        "FAR_left": None,
                        "FAR_right": None,
                        "FFPM": None
                    })
        df_result = pd.concat([df_result, result_row.to_frame().T], ignore_index=True)
        
    # FusionCatcher
    for index, row in df_fusioncatcher.iterrows():
        result_row = pd.Series({  
                        "caller": "fusion catcher", 
                        "gene1": row['Gene_1_symbol(5end_fusion_partner)'], 
                        "gene2": row['Gene_2_symbol(3end_fusion_partner)'], 
                        "chrom1": row['Fusion_point_for_gene_1(5end_fusion_partner)'].split(":")[0],
                        "position1": row['Fusion_point_for_gene_1(5end_fusion_partner)'].split(":")[1],
                        "chrom2": row['Fusion_point_for_gene_2(3end_fusion_partner)'].split(":")[0],
                        "position2": row['Fusion_point_for_gene_2(3end_fusion_partner)'].split(":")[1],
                        "spanning_reads": row['Counts_of_common_mapping_reads'],
                        "spanning_pairs": row['Spanning_unique_reads'],	
                        "coverage1": None, 
                        "coverage2": None, 
                        "annotation": row['Fusion_description'], 
                        "confidence": None, 
                        "prediction_effect": row['Predicted_effect'],
                        "anchor": None,
                        "FAR_left": None,
                        "FAR_right": None,
                        "FFPM": None
                    })
        df_result = pd.concat([df_result, result_row.to_frame().T], ignore_index=True)
        
    # Cicero
    for index, row in df_cicero.iterrows():
        result_row = pd.Series({  
                        "caller": "cicero", 
                        "gene1": row['geneA'], 
                        "gene2": row['geneB'], 
                        "chrom1": row['chrA'],
                        "position1": row['posA'],
                        "chrom2": row['chrB'],
                        "position2": row['posB'],
                        "spanning_reads": row['readsA'] + row['readsB'],
                        "spanning_pairs": row['matchA'] + row['matchB'],	
                        "coverage1": row['coverageA'], 
                        "coverage2": row['coverageB'], 
                        "annotation": None, 
                        "confidence": row['rating'], 
                        "prediction_effect": row['featureA'] + ", " + row['featureB'],
                        "anchor": None,
                        "FAR_left": None,
                        "FAR_right": None,
                        "FFPM": None
                    })
        df_result = pd.concat([df_result, result_row.to_frame().T], ignore_index=True)
        
    # FusionInspector
    for index, row in df_fusioninspector.iterrows():
        result_row = pd.Series({  
                        "caller": "fusioninspector", 
                        "gene1": row['#FusionName'].split('--')[0], 
                        "gene2": row['#FusionName'].split('--')[1], 
                        "chrom1": row['LeftBreakpoint'].split(":")[0],
                        "position1": row['LeftBreakpoint'].split(":")[1], 
                        "chrom2": row['RightBreakpoint'].split(":")[0], 
                        "position2": row['RightBreakpoint'].split(":")[1], 
                        "spanning_reads": row['SpanningFragCount'],
                        "spanning_pairs": row['JunctionReadCount'],	
                        "coverage1": None, 
                        "coverage2": None, 
                        "annotation": row['annots'], 
                        "confidence": None, 
                        "prediction_effect": row['SpliceType'], 
                        "anchor": row['LargeAnchorSupport'],
                        "FAR_left": row['FAR_left'],
                        "FAR_right": row['FAR_right'],
                        "FFPM": row['FFPM']
                    })
        df_result = pd.concat([df_result, result_row.to_frame().T], ignore_index=True)

    # remove 'chr' from chrom fields if present ('chr1' -> '1')
    df_result['chrom1'] = df_result['chrom1'].map(lambda x: x.lstrip('chr'))
    df_result['chrom2'] = df_result['chrom2'].map(lambda x: x.lstrip('chr'))
    # convert position columns to numeric
    df_result[["position1", "position2"]] = df_result[["position1", "position2"]].apply(pd.to_numeric, errors='ignore')
    # sort rows by by chromosomes and by p
    df_result = df_result.sort_values(by = ['gene1', 'gene2'])
        
    df_result.to_csv(outfile+'.csv', index=False)
    excel_formatted_table(df_result, outfile)
        
def excel_formatted_table(df_result, outfile):
    
    with pd.ExcelWriter(outfile+".xlsx", engine='xlsxwriter') as writer:
        df_result.to_excel(writer, sheet_name='Fusion Genes', index=False) 
        (max_row, max_col) = df_result.shape
        workbook  = writer.book
        worksheet = writer.sheets['Fusion Genes']
        
        worksheet.freeze_panes(1, 0)
        
        format_red_row = workbook.add_format({'bg_color': '#FFCCCC'})
        format_green_row = workbook.add_format({'bg_color': '#CCFFCC'})
        format_red_cell = workbook.add_format({'bg_color': '#F2B5B5'})
        format_green_cell = workbook.add_format({'bg_color': '#A9F2B3'})
        format_yellow = workbook.add_format({'bg_color': '#FFFFCC'})
        format_blue = workbook.add_format({'bg_color': '#90c4fc'})
        
        # highlight fusioninspector
        worksheet.conditional_format(1, 0, max_row, 0,
                                {'type':     'cell',
                                'criteria': 'equal to',
                                'value': 'fusioninspector',
                                'format':   format_blue})
        
        # spanning reads > 50
        worksheet.conditional_format(1, 7, max_row, 7,
                                {'type':     'formula',
                                'criteria': '=AND(H2>50,H2<>"")',
                                'value': 50,
                                'format':   format_green_cell})
        
        # spanning reads < 5
        worksheet.conditional_format(1, 7, max_row, 7,
                                {'type':     'formula',
                                'criteria': '=AND(H2<5,H2<>"")',
                                'value': 5,
                                'format':   format_red_cell})
        
        # coverage1 < 50
        worksheet.conditional_format(1, 9, max_row, 9,
                                {'type':     'formula',
                                'criteria': '=AND(J2<50,J2<>"")',
                                'value': 50,
                                'format':   format_red_cell})
        
        # coverage2 < 50
        worksheet.conditional_format(1, 10, max_row, 10,
                                {'type':     'formula',
                                'criteria': '=AND(K2<50,K2<>"")',
                                'value': 50,
                                'format':   format_red_cell})
        
        # same chromosomes
        worksheet.conditional_format(1, 3, max_row, 3,
                                {'type':     'cell',
                                'criteria': 'equal to',
                                'value': '$F2',
                                'format':   format_yellow})
        
        worksheet.conditional_format(1, 5, max_row, 5,
                                {'type':     'cell',
                                'criteria': 'equal to',
                                'value': '$D2',
                                'format':   format_yellow})
            
        # prediction efect
        prediction_efect = ['intragenic', 'intron', 'out-of-frame', 'INCL_NON_REF_SPLICE', 'False, False']
        
        for efect in prediction_efect:
            worksheet.conditional_format(1, 0, max_row, max_col-1,
                                    {'type':     'formula',
                                    'criteria': '=ISNUMBER(SEARCH("'+efect+'",$N2))',
                                    'format':   format_red_row})
            
        # relevance of annotations
        not_relevant_anot = ['GTEx_recurrent_StarF2019', 'BodyMap', 'DGD_PARALOGS', 'HGNC_GENEFAM', 'Greger_Normal', 'Babiceanu_Normal', 'ConjoinG']
        # 'Yes' in Jaffa rows stands for Mitelman
        relevant_annot = ['Yes', 'Mitelman', 'chimerdb_omim', 'chimerdb_pubmed', 'ChimerKB', 'ChimerPub', 'ChimerSeq', 'Cosmic', 'YOSHIHARA_TCGA', 'Klijn_CellLines', 'Larsson_TCGA', 'CCLE', 'HaasMedCancer', 'GUO2018CR_TCGA', 'TumorFusionsNAR2018', 'TCGA_StarF2019', 'CCLE_StarF2019', 'DEEPEST2019']
        
        for annot in not_relevant_anot:
            worksheet.conditional_format(1, 0, max_row, max_col-1,
                                    {'type':     'formula',
                                    'criteria': '=ISNUMBER(SEARCH("'+annot+'",$L2))',
                                    'format':   format_red_row})
        
        for annot in relevant_annot:
            worksheet.conditional_format(1, 0, max_row, max_col-1,
                                    {'type':     'formula',
                                    'criteria': '=ISNUMBER(SEARCH("'+annot+'",$L2))',
                                    'format':   format_green_row})
            
        # low confidence
        low_confidence = ['LowConfidence', 'LQ', 'low']
        
        for conf in low_confidence:
            worksheet.conditional_format(1, 0, max_row, max_col-1,
                                    {'type':     'formula',
                                    'criteria': '=ISNUMBER(SEARCH("'+conf+'",$M2))',
                                    'format':   format_red_row})
        
if __name__ == "__main__":
   parser = argparse.ArgumentParser()
   parser.add_argument('-a', '--arriba')
   parser.add_argument('-f', '--fusioncatcher')
   parser.add_argument('-s', '--starfusion')
   parser.add_argument('-j', '--jaffa')
   parser.add_argument('-c', '--cicero')
   parser.add_argument('-i', '--fusioninspector')
   parser.add_argument('-o', '--outfile')
   args = parser.parse_args() 
   create_final_result_table(args.arriba, args.fusioncatcher, args.starfusion, args.jaffa, args.cicero, args.fusioninspector, args.outfile)

