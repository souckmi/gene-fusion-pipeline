#!/bin/bash

STAR --genomeDir ${params.reference}/Arriba2/STAR_index_* \
     --readFilesIn ${reads1} ${reads2} \
     --runThreadN ${task.cpus} \
     --outReadsUnmapped None \
     --twopassMode Basic \
     --readFilesCommand "gunzip -c" \
     --outSAMstrandField intronMotif \
     --outSAMunmapped Within \
     --chimSegmentMin 12  \
     --chimJunctionOverhangMin 8 \
     --chimOutJunctionFormat 1 \
     --alignSJDBoverhangMin 10 \
     --alignMatesGapMax 100000 \
     --alignIntronMax 100000 \
     --alignSJstitchMismatchNmax 5 -1 5 5 \
     --outSAMattrRGline ID:GRPundef \
     --chimMultimapScoreRange 3 \
     --chimScoreJunctionNonGTAG -4 \
     --chimMultimapNmax 20 \
     --chimNonchimScoreDropMin 10 \
     --peOverlapNbasesMin 12 \
     --peOverlapMMp 0.1 \
     --alignInsertionFlush Right \
     --alignSplicedMateMapLminOverLmate 0 \
     --alignSplicedMateMapLmin 30 \
     --outSAMtype BAM Unsorted \
     --chimOutType Junctions WithinBAM \
     --outBAMcompression 0
