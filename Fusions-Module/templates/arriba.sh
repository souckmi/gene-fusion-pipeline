#!/bin/bash

/arriba*/arriba \
	-x ${bam_file} \
	-o fusions.tsv -O fusions.discarded.tsv \
	-a ${params.reference}/Arriba2/*.fa \
    -g ${params.reference}/Arriba2/*.gtf \
    -b ${params.reference}/Arriba2/database/blacklist_*.tsv.gz \
    -k ${params.reference}/Arriba2/database/known_fusions_*.tsv.gz \
    -p ${params.reference}/Arriba2/database/protein_domains_*.gff3 \
	-U 100
