#!/bin/bash

mkdir -p fusioncatcher
/opt/fusioncatcher/v1.30/bin/fusioncatcher.py -i . \
                                              -d ${params.reference}/FusionCatcher/human_v102 \
                                              -o fusioncatcher \
                                              -p ${task.cpus}
