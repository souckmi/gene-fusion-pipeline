#!/bin/bash

samtools sort ${bam_file} \
         -o sorted_${bam_file.baseName}.bam \
         -@ ${task.cpus}

samtools index -b sorted_${bam_file.baseName}.bam \
               -o sorted_${bam_file.baseName}.bam.bai \
               -@ ${task.cpus}