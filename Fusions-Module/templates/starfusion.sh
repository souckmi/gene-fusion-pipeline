#!/bin/bash

mkdir -p starfusion
STAR-Fusion --genome_lib_dir ${params.reference}/STAR-Fusion/STARFusion/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play/ctat_genome_lib_build_dir \
            -J ${junctions} \
            --output_dir starfusion \
            --CPU ${task.cpus}
