#!/bin/bash

Cicero.sh -b ${bam_file} \
          -g GRCh38_no_alt \
          -r ${params.reference+"/Cicero"} \
          -o . 