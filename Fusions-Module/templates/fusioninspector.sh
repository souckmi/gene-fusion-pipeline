#!/bin/bash

mkdir -p fusioninspector
FusionInspector --fusions ${arriba},${fusioncatcher},${starfusion},${jaffa},${cicero} \
                --genome_lib ${params.reference}/STAR-Fusion/STARFusion/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play/ctat_genome_lib_build_dir \
                --left_fq ${reads1} \
                --right_fq ${reads2} \
                -O fusioninspector \
                --vis \
                --examine_coding_effect \
                --annotate \
                --extract_fusion_reads_file fusion_reads_fi

