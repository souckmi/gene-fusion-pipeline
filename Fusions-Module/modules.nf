#!/usr/bin/env nextflow

// Using DSL-2
nextflow.enable.dsl=2

/*
 * Preprocessing - fastp
 */
process fastp {

    publishDir "$params.outdir/$pair_id/fastp", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_fastp_${filename.split('/')[1]}" }, mode: 'copy'
    tag "$pair_id"

    input:
    tuple val(pair_id), path(reads)

    output:
    tuple val(pair_id), path("fastp/demultiplexed_trimmed_*1.fastq.gz"), path("fastp/demultiplexed_trimmed_*2.fastq.gz"), emit: trimmed_reads
    path "fastp/*"
       
    script:
    template 'fastp.sh'
}


/*
 * STAR
 */
process star {

    publishDir "$params.outdir/$pair_id/star", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_star_$filename" }, mode: 'copy'
    tag "$pair_id"

    input:
    tuple val(pair_id), path(reads1), path(reads2)

    output:
    tuple val(pair_id), path("Chimeric.out.junction"), emit: junctions
    tuple val(pair_id), path("Aligned.out.bam"), emit: bam_file
    path "*"

    script:
    template 'star.sh'
}

/*
 * SAMTOOLS
 */
process samtools {

    publishDir "$params.outdir/$pair_id/samtools", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_samtools_$filename" }, mode: 'copy'
    tag "$pair_id"

    input:
    tuple val(pair_id), path(bam_file)

    output:
    tuple val(pair_id), path("sorted*.bam"), path("sorted*.bai"), emit: result
    path "*"

    script:
    template 'samtools.sh'
}
 
/*
 * JAFFA
 */
process jaffa {

    publishDir "$params.outdir/$pair_id/jaffa", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_$filename" }, mode: 'copy'
    tag "$pair_id"

    input:
    tuple val(pair_id), path(reads1), path(reads2)

    output:
    tuple val(pair_id), path("*.csv"), emit: result
    path "*"

    script:
    template 'jaffa.sh'
}

/*
 * FUSIONCATCHER
 */
process fusioncatcher {

    publishDir "$params.outdir/$pair_id/fusioncatcher", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_fusioncatcher_${filename.split('/')[1]}" }, mode: 'copy'
    tag "$pair_id"

    input:
    tuple val(pair_id), path(reads1), path(reads2)

    output:
    tuple val(pair_id), path("fusioncatcher/final-list_candidate-fusion-genes.txt"), emit: result
    path "fusioncatcher/*"
       
    script:
    template 'fusioncatcher.sh'
}

/*
 * ARRIBA
 */
process arriba {

    publishDir "$params.outdir/$pair_id/arriba", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_arriba_$filename" }, mode: 'copy' 
    tag "$pair_id"

    input:
    tuple val(pair_id), path(bam_file)

    output:
    tuple val(pair_id), path("fusions.tsv"), emit: result
    path "*.tsv"

    script:
    template 'arriba.sh'
}

/*
 * STAR-FUSION
 */
process starfusion {

    publishDir "$params.outdir/$pair_id/starfusion", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_starfusion_${filename.split('/')[1]}" },  mode: 'copy'  
    tag "$pair_id"

    input:
    tuple val(pair_id), path(junctions)

    output:
    tuple val(pair_id), path("starfusion/star-fusion.fusion_predictions.tsv"), emit: result
    path "starfusion/*"
       
    script:
    template 'starfusion.sh'
}

/*
 * CICERO
 */
process cicero {

    publishDir "$params.outdir/$pair_id/cicero", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_cicero_$filename" },  mode: 'copy'  
    tag "$pair_id"

    input:
    tuple val(pair_id), path(bam_file), path(bai_file)

    output:
    path "*.{err,out,log}"
    path "CICERO_DATADIR/*/*.txt"
    tuple val(pair_id), path("CICERO_DATADIR/${bam_file[0].baseName}/final_fusions.txt"), emit: result

    script:
    template 'cicero.sh'
}

/*
 * FUSION TABLES for fusioninspector
 */
process fusion_tables { 
    
    input:
    tuple val(pair_id), path(reads1), path(reads2), path(jaffa), path(fusioncatcher), path(starfusion), path(arriba), path(cicero)
    tag "$pair_id"

    output:
    tuple val(pair_id), path(reads1), path(reads2), path("arriba.txt"), path("fusioncatcher.txt"), path("starfusion.txt"), path("jaffa.txt"), path("cicero.txt"), emit: result_fusion_names
       
    """
    fusion_tables.py -a ${arriba} -j ${jaffa} -f ${fusioncatcher} -s ${starfusion} -c ${cicero}
    """
}

/*
 * FUSIONINSPECTOR
 */
process fusioninspector {

    publishDir "$params.outdir/$pair_id/fusioninspector", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_fusioninspector_${filename.split('/')[1]}" }, mode: 'copy'  
    tag "$pair_id"

    input:
    tuple val(pair_id), path(reads1), path(reads2), path(arriba), path(fusioncatcher), path(starfusion), path(jaffa), path(cicero)

    output:
    path "fusioninspector/*"
    tuple val(pair_id), path("fusioninspector/finspector.FusionInspector.fusions.abridged.tsv.annotated.coding_effect"), emit: result
       
    script:
    template 'fusioninspector.sh'
}

/*
 * DRAW FUSIONS Arriba script
 */
process draw_fusions {
    publishDir "$params.outdir/$pair_id/fusioninspector/visualization", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_fusioninspector_$filename" }, mode: 'copy'  
    tag "$pair_id"

    input: 
    tuple val(pair_id), path(fusions), path(bam), path(bai)

    output:
    path "*.pdf"

    script:
    template 'draw_fusions_arriba.sh'

}

/*
 * FINAL RESULT
 */
process final_result_table {

    publishDir "$params.outdir/$pair_id", saveAs: { filename -> "${pair_id}_${params.runId}_${params.referenceVersion}_$filename" }, mode: 'copy'  
    tag "$pair_id"
    
    input: 
    tuple val(pair_id), path(reads1), path(reads2), path(jaffa), path(fusioncatcher), path(starfusion), path(arriba), path(cicero), path(fusioninspector)

    output:
    path("final_result_table.*")

    """
    final_result_table.py -a ${arriba} -j ${jaffa} -f ${fusioncatcher} -s ${starfusion} -c ${cicero} -i ${fusioninspector} -o final_result_table
    """
}

/*
 * FUSIONS DETECTION WORKFLOW
 */
workflow fusion_detection{
    take: data

    main:

    // preprocessing
    fastp(data)

    // align, index
    star(fastp.out.trimmed_reads)
    samtools(star.out.bam_file)

    // fusions detection
    jaffa(fastp.out.trimmed_reads)
    fusioncatcher(fastp.out.trimmed_reads)
    starfusion(star.out.junctions)
    arriba(star.out.bam_file)
    cicero(samtools.out.result)

    // results join
    fastp.out.trimmed_reads
        .join(jaffa.out.result)
        .join(fusioncatcher.out.result)
        .join(starfusion.out.result)
        .join(arriba.out.result)
        .join(cicero.out.result)
        .set{joined_results_ch}

    // validation
    fusion_tables(joined_results_ch)
    fusioninspector(fusion_tables.out.result_fusion_names)

    //fusioninspector visualizatin
    fusioninspector.out.result  
        .join(samtools.out.result)
        .set { fusions_viz_ch }

    draw_fusions(fusions_viz_ch)

    //result table
    joined_results_ch
        .join(fusioninspector.out.result)
        .set{ joined_final_results_ch }

    final_result_table(joined_final_results_ch)
}
